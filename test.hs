{-# LANGUAGE NamedFieldPuns #-}

import LysKom
import Control.Monad.IO.Class (liftIO)

main :: IO ()
main = do
    sock <- lyskomConnect "kom.lysator.liu.se" "lyskom" "hugo%gandalf.adrift.space"
    runKom sock (putStrLn . ("Async: " ++) . show)  $ do

        (ConfZInfo { confNo = pn }):_ <- lookupZName "Hugo Hörnquist" True False
        -- let pn = confNo . head $ x
        login4 pn "PASSWORD GOES HERE" True
        x <- lookupZName "Root (@) Lysator" False True
        let cn = confNo . head $ x
        resp <- queryReadTexts11 pn cn False 0
        liftIO (putStrLn . show $ resp)

    putStrLn "bye"

