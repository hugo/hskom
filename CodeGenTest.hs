
import Language.Haskell.TH (runQ, pprint)
import LysKom.ProtocolA (documentParser)
import Text.ParserCombinators.Parsec (parseFromFile)
import System.Directory (doesFileExist)

import LysKom.Internal.CodeGenerator

-- similar to (\x -> head . filter x), but works on infinite lists
findM :: Monad m => (a -> m Bool) -> [a] -> m a
findM p (x:xs) = p x >>= \v -> if v then return x else findM p xs

isFilenameAvailable :: FilePath -> IO Bool
isFilenameAvailable path = doesFileExist path >>= return . not

-- returns a unused filename
newfile :: String -> String -> IO FilePath
newfile base ext = findM isFilenameAvailable (basename : map fmt [0..])
        where fmt i    = base ++ show i ++ "." ++ ext
              basename = base ++ "." ++ ext

main :: IO ()
main = do

    let protApath = "/usr/share/doc/lyskom/protocol-a-full.txt"

    -- I know, I know, time to check to time to use race condition
    parseResult   <- newfile "parse-result"  "hs"
    generatedCode <- newfile "generatedCode" "hs"

    -- this parses the file twice, but it's fast enough, and we don't
    -- care about performance here
    d <- parseFromFile documentParser protApath
    case d of
        Left err ->  putStrLn $ "Parsing failed: " ++ show err
        Right lst -> writeFile parseResult . unlines . map show $ lst

    runQ (spliceGeneratedCode protApath) >>= (writeFile generatedCode) . pprint

    putStrLn $ "Wrote " ++ parseResult ++ " and " ++ generatedCode
