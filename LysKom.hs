{-# LANGUAGE DeriveAnyClass
           , OverloadedStrings
           , ImportQualifiedPost
           , TemplateHaskell
           , DuplicateRecordFields
           , NamedFieldPuns
           #-}

module LysKom
    {-( runKom
    , lyskomConnect
    , LysKom
    , request
    , KomError
    )-} where

import Control.Exception (Exception, throw)
import Control.Monad (forever)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.State.Strict (StateT, runStateT, get, put)
import Control.Concurrent (forkIO)
import Control.Concurrent.STM
    ( atomically
    , TChan
    , newTChan
    , readTChan
    , tryReadTChan
    , writeTChan
    )
import Data.Attoparsec.ByteString
import Data.Attoparsec.ByteString.Char8
import Data.ByteString.Builder hiding (char8)
import Data.ByteString.Lazy (toStrict)
import Data.ByteString qualified as B
import Data.Functor ((<&>))
import Data.Maybe (fromMaybe)
import LysKom.ErrorCodes
import LysKom.WireFormat
import Network.Socket
import Network.Socket.ByteString (send, recv)

import Control.Monad.Catch (catch)

--------------------------------------------------

import LysKom.Internal.CodeGenerator (spliceGeneratedCode)

--------------------------------------------------

spliceGeneratedCode "/usr/share/doc/lyskom/protocol-a-full.txt"

data KomType = KomType
    { refno     :: Int
    , socket_   :: Socket
    , chan      :: TChan B.ByteString
    , handler   :: KomAsync -> IO ()
    }

type LysKom a = StateT KomType IO a

-- NOTE In theory KomError shouldn't be exported since neither
-- ParseError nor ProtocolError is sensible for the user to create,
-- KomError (the constructor) is however sensible to export since the
-- dummy error-code "OtherError String" exists, for user defined
-- errors.
data KomError = KomError ErrorCode Int                   -- expected runtime errors
              | ParseError B.ByteString [String] String  -- attoparsec failed, should never happen
              | ProtocolError String                     -- Fatal error with LysKom, should never happen
              deriving (Show, Exception)

getRefNo :: LysKom Int
getRefNo = do
    ref <- get
    let r = refno ref
    put $ ref { refno = r + 1 }
    return r

getSock :: LysKom Socket
getSock = socket_ <$> get

data PacketHeader = PacketSuccess Int
                  | PacketError Int Int Int
                  | PacketProtocolError String


-- can't simply parse to end of line here, since hollith:s might
-- contain literal linefeeds
parseSuccess :: Parser PacketHeader
parseSuccess = fmap PacketSuccess $ char '=' *> decimal

parseProtocolError :: Parser PacketHeader
parseProtocolError = fmap PacketProtocolError $ string "%%" *> manyTill anyChar (char '\n')

parsePacketError :: Parser PacketHeader
parsePacketError = do
    char '%'
    refNo <- decimal
    char ' '
    errorCode <- decimal
    char ' '
    errorStatus  <- decimal
    char '\n'
    return $ PacketError refNo errorCode errorStatus

parseHelp :: WireFormat a => Parser a -> Parser (Either KomError a)
parseHelp p = choice
    [ (parseSuccess *> p) <&> Right
    , parseProtocolError <&> \(PacketProtocolError m) -> Left . ProtocolError $ m
    , parsePacketError <&> \(PacketError _ c s) -> Left $ KomError (toEnum c) s ]

ii = 5 :: Int

parseInput :: WireFormat a => Parser a -> Parser ([KomAsync], (Either KomError a))
parseInput p = do
    -- We NEED to handle asyncs here, since otherwise we run the risk
    -- of an async arriving in the few timeincrements between checking
    -- for asyncs and checking for messages
    as <- many' (char ':' *> decoder <* char '\n')
    ps <- parseHelp p
    return (as, ps)

getSocket :: HostName -> ServiceName -> IO (Socket, SockAddr)
getSocket addr port = do
    ad:_ <- getAddrInfo (Just hints) (Just addr) (Just port)
    sock <- socket (addrFamily ad) (addrSocketType ad) (addrProtocol ad)
    return (sock, addrAddress ad)
    where hints = defaultHints { addrSocketType = Stream }


-- Connect to a given LysKom server, and perform the initiial
-- handshake. Returns a LysKom instance.

lyskomConnect :: HostName -> String -> String -> IO Socket
lyskomConnect server port whoami = do
    (sock, addr) <- getSocket server port
    connect sock addr
    send sock $ toStrict $ toLazyByteString $ char7 'A' <> encode whoami
    ret <- recv sock 7
    if not (ret == "LysKOM\n")
        then error $ "Socket not speaking LysKom" ++ show ret
        else return sock

runKom' :: KomType -> LysKom a -> IO (a, KomType)
runKom' kt kom = runStateT kom kt

-- "Regular" error handler. Takes a procudere which produces a defalut
-- value from the given error.
withKomError :: (ErrorCode -> Int -> a) -> LysKom a -> LysKom a
withKomError handler body = body `catch` f
    where f (KomError errorCode status) = return $ handler errorCode status
          f other = throw other

-- Moves errors in LysKom from thrown exceptions to explicit Eithers.
tryKom :: LysKom b -> LysKom (Either (ErrorCode, Int) b)
tryKom body = (Right <$> body) `catch` f
    -- unwrap error since it's only KomErrors which are sensible for
    -- the client to catch (and handle)
    where f (KomError e s) = return $ Left (e, s)
          f other          = throw other

mkKom :: Socket -> (KomAsync -> IO ()) -> IO KomType
mkKom sock h = do
    chan <- atomically newTChan
    forkIO $ forever $ do
        bs <- recv sock 1024
        atomically $ writeTChan chan bs
    return $ KomType 0 sock chan h

pumpAsyncs :: LysKom ()
pumpAsyncs = do
    KomType { handler, chan } <- get
    -- TODO we possibly want to save the remaining bytestring we get
    -- from the chan somewhere. Either by pushing it back into the
    -- channel, or separately in the KomType record. This to not drop
    -- any data when reading from different locations.
    res <- liftIO
        $ parseWith (atomically $ fromMaybe "" <$> tryReadTChan chan)
                    (many' (char ':' *> decoder <* char '\n'))
                    ""
    case res of
        Fail bs ctxs err -> throw $ ParseError bs ctxs err
        Done bs as -> liftIO $ do
            liftIO $ mapM_ handler as

request :: WireFormat a => Builder -> LysKom a
request builder = do
    refNo <- getRefNo
    let msg = intDec refNo <> char7 ' ' <> builder <> char7 '\n'

    let bs = toStrict $ toLazyByteString msg

    sock <- getSock
    liftIO $ send sock $ bs

    KomType { handler, chan } <- get

    res <- liftIO $ parseWith (atomically $ readTChan chan) (parseInput (many' (char8 ' ') *> decoder <* char '\n')) ""
    case res of
        Fail bs' ctxs err -> throw $ ParseError bs' ctxs err
        Done bs' (as, Left err) -> liftIO $ do
            mapM_ handler as
            throw err
        Done bs' (as, Right m) -> do
            liftIO (mapM_ handler as)
            return m



