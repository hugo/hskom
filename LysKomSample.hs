--
-- THIS FILE IS FOR TESTING USAGES ONLY. DO NOT USE IN REAL CODE
-- 
-- Simple file for running kom commands from an ghci prompt.
-- example:
-- $ ghcp LysKomSample.hs
-- > setupKom "User Name" "Hunter2"
-- > k (lookupZName "User Name" True False

import LysKom

import Control.Concurrent.MVar 
    ( MVar
    , newMVar
    , takeMVar
    , putMVar)
import Control.Exception (throw, AssertionFailed(AssertionFailed))
import Data.Maybe (fromMaybe)
import Network.Hostname (fqdn)
import System.Environment (lookupEnv)
import System.IO.Unsafe (unsafePerformIO)
import System.Posix.User (getLoginName)


komVar :: MVar (Maybe KomType)
komVar = unsafePerformIO $ newMVar Nothing

type Username = String
type Password = String

setupKom :: Username -> Password -> IO ()
setupKom username password = do
    server <- fromMaybe "kom.lysator.liu.se" <$> lookupEnv "KOMSERVER"
    user <- getLoginName
    host <- fqdn
    sock <- lyskomConnect server "lyskom" (user ++ "%" ++ host)
    kom <- mkKom sock (putStrLn . show)
    runKom' kom $ do
        ConfZInfo { confNo = personNo }:_ <- lookupZName username True False
        login personNo password False
    putMVar komVar $ Just kom

k :: LysKom a -> IO a
k op = do
    mKom <- takeMVar komVar
    kom <- case mKom of
        Nothing -> throw . AssertionFailed $  "Run setupKom first"
        Just k -> return k
    (r, kom') <- runKom' kom op
    putMVar komVar $ Just kom'
    return r
