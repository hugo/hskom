\begin{comment}
\begin{code}
module Datatype where

import Text.ParserCombinators.Parsec
import Data.Char (digitToInt)
import Text.Parsers.Helpers
import AstHaskell
\end{code}
\end{comment}

This section details the primitive datatypes of the protocol.

It also declares parsers to go from incomming data to haskell values.

\TODO{Refer to ``Parsing Types''}

\subsection{Integers}

Integers are represented as base 10 ASCII strings, and have
the variations INT32, INT16, INT8, \& BOOL. Which contain 32,
16, 8, \& 1 Bits of data respectivly.

For the 32, 16, \& 8 variants a simple read can be used, note
however that a check that it isn't larger than expected might
be a good thing we have.

My current parser is simple to take read a series of digits.
Note that this never checks if the number is the correct size

\begin{code}
-- intParser was moved to Helpers.lhs
-- because int's needed to be parsed at quite a few places.
\end{code}

And while booleans could have the same parser, we might as
well also run it through `toEnum' to get a haskell boolean
to work with.

\begin{code}
boolParser :: GenParser Char () Bool
boolParser = (return $ toEnum . digitToInt)
          <*> oneOf ['0', '1']
\end{code}

Floats works like integers, except they have a decimal place.
For the time being, refer to C's `printf("\%g", val);' for
formatting of floats.

I belive that floats follows the regex rule

\begin{verbatim}
[+-]?(\d+|\d*\.\d+)([eE](-|+?)\d+)
\end{verbatim}

which isn't the easiest to express in parsec.

\subsection{Bit Strings}

Sequence of ASCII `0' \& `1', behaves like many boolean values
packed together without spaces between. The number of expected
values are showed in the specification, as well as what each bit
represents.

\begin{code}
bitstringParser :: GenParser Char () [Bool]
bitstringParser = many boolParser
\end{code}

\subsection{Enumerations}

Enum values are represented as INT32, and the actual enum names
are presented in the BNF.

The notation parser contains a parser for parsing enumeration
deffinitions. There is however currently no way to actually
go between the enumeration value and an integer. This will be
solved once I can build parsers from the BNF.

\subsection{Arrays}

Arrays are generic types for storing items. And require a type
when they are declared.
The form for arrays is "<N> \{ <elem> <elem> ... \}", where <N>
is the length of the array, and each <elem> is an item of the
type of the array.

The empty array is represented as either "0 *" or "0 \{ \}". Note
that the client must always send empty arrays using the secound
variant. And that the server probably always use the first form.
The secound form can also be transmitted by the server if the
client requests the size of an array without its contents.

Note that if an array without a body, for example "19 *" is parsed
then the value returned is "Right (19, [])". It's therefor up to
the program to understand that the empty array here is not an error,
but rather what's requested. This could be solved by wrapping the
list in Maybe. But that would come at the expense of another nested
monad.

Currently all sub arrays need to have the same type. I don't believe
that protocol A ever uses nested arrays directly, but if it
does this has to be updated.

\begin{code}
emptyArrayParser = char '*' *> return []
asListBody = between (string "{ ") (char '}')

arrayParser :: GenParser Char () a -> GenParser Char () (Int, [a])
arrayParser subParser = do
    len <- intParser <* space
    items  <- emptyArrayParser
          <|> asListBody (count len $ (subParser <* space))
          <?> "LysKOM Array"
    return (len, items)
\end{code}

\subsection{Hollerith}
\label{item:holler}

Hollerith's are a type of strings defined as: <num>H<str>
where <num> is the number of bytes in the string <str>.

The following parser works by reading any number of characters
which isn't `H', this should possibly instead be any number of
digits. It then reads the literal letter `H', followed by a
string of length <num>, containing any characters.

NOTE that this uses haskell characters, while LysKOM expects
a bytestring.

\TODO{data Hollerith is moved to AstHaskell.lhs,
it also shouldn't have a special instance of show,
but rather a special typeclass we derive}

\begin{code}
data Hollerith = Hollerith Int String
instance Show Hollerith where
    show (Hollerith size str) = show size ++ "H" ++ str

hollerithParser :: GenParser Char () Hollerith
hollerithParser = do
    num <- intParser <* char 'H'
    str <- count num anyChar
    return $ Hollerith num str
\end{code}

\TODO{everything in this section from here on should
\emph{probably} be moved to the Temlate chapter}

\subsection{Structure}

One of the most common compound data types is the structure.

A structure is on the form

\begin{verbatim}
    ( name : type ;
      name : type ; )
\end{verbatim}

\TODO{MORE, also, last `;' optional}

\subsection{Selections}

If my understanding is correct selections is a form of union
types.

A selection is on the form

\begin{verbatim}
    <name> ::= SELECTION (
        <n>=<name>      <fieldName> : <type>;
    )
\end{verbatim}

Where <n> is a number, and messages over the protocol sends
this number telling which instance of the selection is used.

<fieldName> is usually similar to <name>, according to the
documentation. I however don't know why there are two
different fields. Or why one should be used over the other.

The example of a SELECTION given in the documentation is

\begin{verbatim}
    description ::= SELECTION (
        1=name           the_name : HOLLERITH;
        2=age            years    : INT32;
    )
\end{verbatim}

And then notes that "two legal messages of the type
`description' are `1 4HJohn' and `2 18'."

Ideally, each SELECTION type should have its own parser
defined, Which probably should return a `data' type
which instance is the <name>, and which value is of
type <type>. Throwing away <fieldName> and having <n>
implicit as a derivition from Enum.

The sample implementatino for the above mention
`descrption' type should therefor be

\begin{code}
data Description
    = Name Hollerith
    | Years Int
    deriving (Show)
\end{code}

\TODO{
    the above declaration should probably not actually be
    code which is run.
}
