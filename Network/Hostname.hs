module Network.Hostname
( Hostname(Hostname,host,domain)
, getHostname
, fqdn
) where

import System.Process

data Hostname = Hostname
  { host :: String
  , domain :: String
  } deriving (Show)

hostname :: IO String
hostname = head . lines <$> readCreateProcess (shell "hostname") ""

domainname :: IO String
domainname = head . lines <$> readCreateProcess (shell "hostname --domain") ""

getHostname :: IO Hostname
getHostname = do
    host <- hostname
    domain <- domainname
    return $ Hostname host domain

fqdn :: IO String
fqdn = do
    h <- getHostname
    return $ host h ++ "." ++ domain h
