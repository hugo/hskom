\section{Types}

\begin{code}
module LysKom.ProtocolA.Types where
\end{code}

\begin{comment}
\begin{code}
import Text.ParserCombinators.Parsec
import LysKom.ProtocolA.Helpers
    ( whitespaces
    , word
    , withWS
    , withDelim
    , intParser
    )
import Data.Functor ((<&>), ($>))
\end{code}
\end{comment}

The protocol definition can be grouped into the following types of data:

\begin{code}
data ProtocolAItem =
\end{code}

Comments
\begin{code}
    Comment String
\end{code}

Declarations of new types. Anything on the form $key ::= value$

\begin{code}
    | DerivedType String LysType
\end{code}

Requst and async declarations, which specifies how data is transmitted to and from the server.

\begin{code}
    | Request String Int [(String, LysType)] (Maybe LysType)
    | Async String Int [(String, LysType)]
\end{code}

A number of different metadata types, which I mostly ignore

\begin{code}
    | ProtoEdition String
    | ProtoVer String
    | LysKomDVersion String
    | TypeAlias String String
    | RequestAlias String String
    | AsyncAlias String String
\end{code}

And finally ``other stuff'', which here is the metadata blocks above each request or async declaration.

\begin{code}
    | Other String
    deriving (Show)
\end{code}

% --------------------------------------------------

During this phase we are \emph{only} interested in the declaration and
names of types, completely ignoring what they actually contain.

% TODO better phrasing
Types are categorized into two types

\begin{code}
data LysType =
\end{code}

The primitive types, which just represent themselves,

\begin{code}
    INT32 | INT16 | INT8 | BOOL | FLOAT | HOLLERITH
\end{code}

and the compound type, which all have some form of argument.

\begin{code}
    | BITSTRING      [String]
    | ENUMERATION    [(String, Int)]
    | ENUMERATION_OF String -- must be name of a selection
    | STRUCTURE      [(String, LysType)]
    | SELECTION      [(Int, String, (String, LysType))]
    | ARRAY          LysType
\end{code}
 .

 In addition to these, we also have type references, which simply
 contains the name of another type,

\begin{code}
    | Reference      String
\end{code}

and type unions.

 \begin{code}
    | Union          (LysType, LysType)
    deriving (Show)
\end{code}

\begin{code}
lysTypeParser :: GenParser Char () LysType
lysTypeParser = do
\end{code}

The simple types, as stated, just represent themselves.

\begin{code}
    t1 <- try (string "INT8")      $> INT8
      <|> try (string "INT16")     $> INT16
      <|> try (string "INT32")     $> INT32
      <|> try (string "BOOL")      $> BOOL
      <|> try (string "FLOAT")     $> FLOAT
      <|> try (string "HOLLERITH") $> HOLLERITH
\end{code}

Each compound type gets its own parser

\begin{code}
      <|> try arrayTypeParser
      <|> try bitstringTypeParser
      <|> try selectionTypeParser
      <|> try enumTypeParser
      <|> try enumOfTypeParser
      <|> try structTypeParser
\end{code}

and if all that fails we fall back to it being the name of another type.

\begin{code}
      <|> (word <&> Reference)
\end{code}

Unions are unfortunately declared through inline pipe characters.
Here we simple append a simple tail parser which captures one extra type at a time.

This however works fine since the only union in the protocol A v.11 is Any-Conf-Type-1.

\begin{code}
    (try (withWS (char '|') *> lysTypeParser)
        <&> \t2 -> Union (t1,t2)) <|> (return t1)
\end{code}

\subsection{Compound Type Parsers}

\subsubsection{Arrays}

Arbitrary length lists of a single type. Specified by

\begin{verbatim}
ARRAY <type>
\end{verbatim}

and parsed as

\begin{code}
arrayTypeParser :: GenParser Char () LysType
arrayTypeParser = fmap ARRAY $ string "ARRAY" *> whitespaces *> lysTypeParser
\end{code}

\begin{code}
listParser :: GenParser Char () a -> GenParser Char () [a]
listParser fieldParser = withDelim "()" (many $ withWS fieldParser)
\end{code}

\subsubsection{Bitstring}
List of boolean. Specified as

\begin{verbatim}
BITSTRING ( name; ... )
\end{verbatim}
where each name is a descriptive string of that bit.

\begin{code}
bitstringTypeParser :: GenParser Char () LysType
bitstringTypeParser = fmap BITSTRING $ string "BITSTRING"
        *> whitespaces
        *> listParser (word <* whitespaces <* char ';')
\end{code}

\subsubsection{Selections}
Tagged unions.

\begin{verbatim}
selection (
    <n>=<name>   <tail> : <type>;
)
\end{verbatim}

% \begin{quote}
% Protocol A: Simple Data Types
%
% given
% \begin{verbatim}
% description ::= SELECTION (
%         1=name           the_name : HOLLERITH;
%         2=age            years    : INT32;
%         )
% \end{verbatim}
% [the] two legal messages of the type `description' are `1 4HJohn' and `2 18'.
% \end{quote}

<name> and <tail> names only for the reader (of the protocol).

\begin{code}
selectionFieldParser :: GenParser Char () (Int, String, (String, LysType))
selectionFieldParser
     =  (,,)
    <$> (intParser <* char '=')
    <*> withWS word
    <*> structFieldParser <* whitespaces

selectionTypeParser :: GenParser Char () LysType
selectionTypeParser = fmap SELECTION
    $ string "SELECTION" *> listParser selectionFieldParser
\end{code}

\subsubsection{Enemurations}
Named subset of integer, equivalent to C. Can either be declared
directly through the ENUMERATION statement\footnote{
    there actually aren't any ENUMERATION statements in protocol A
}, or derived from a
selection through ENUMERATION-OF.

The ``regular'' case looks like
\begin{verbatim}
ENUMERATION ( name  = 1;
              other = 2;
            )
\end{verbatim}

\begin{code}
enumFieldParser :: GenParser Char () (String, Int)
enumFieldParser = (,) <$> (word <* char '=')
                      <*> (intParser <* whitespaces <* char ';')

enumTypeParser :: GenParser Char () LysType
enumTypeParser = ENUMERATION <$> listParser enumFieldParser
\end{code}

Second from is
\begin{verbatim}
ENUMERATION-OF (<selection-type>)
\end{verbatim}
Which builds an enumeration from the <n> and <name> field in
a selection.

\begin{code}
enumOfTypeParser :: GenParser Char () LysType
enumOfTypeParser = fmap ENUMERATION_OF
    $ string "ENUMERATION-OF" *> withDelim "()" word
\end{code}

\subsubsection{Structures}
A sturcture is just a simple compound data type, on the form
\begin{verbatim}
( field-name : TYPE; ... )
\end{verbatim}

\begin{code}
structFieldParser :: GenParser Char () (String, LysType)
structFieldParser = (,)
    <$> (word <* withWS (string ":"))
    <*> lysTypeParser <* whitespaces <* char ';'

structTypeParser :: GenParser Char () LysType
structTypeParser = STRUCTURE <$> listParser structFieldParser
\end{code}
