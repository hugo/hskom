module LysKom.ProtocolA.Helpers where

import Text.ParserCombinators.Parsec

eol :: GenParser Char () String
eol =  try (string "\n\r")
   <|> try (string "\r\n")
   <|> string "\n"
   <|> string "\r"
   <?> "End of Line"

whitespace :: GenParser Char () Char
whitespace = oneOf [' ', '\t', '\n', '\r']

whitespaces :: GenParser Char () String
whitespaces = many whitespace

wordChars :: GenParser Char () Char
wordChars = alphaNum <|> oneOf ['-', '.']

word :: GenParser Char () String
word = many1 wordChars

withWS :: GenParser Char () a -> GenParser Char () a
withWS = between whitespaces whitespaces

withDelim :: String -> GenParser Char () a -> GenParser Char () a
withDelim delims =  between (withWS (char $ delims !! 0))
                            (withWS (char $ delims !! 1))

intParser :: GenParser Char () Int
intParser = return read <*> many digit
