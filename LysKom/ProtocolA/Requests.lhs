\section{Request \& Async Declarations}
\label{item:rpc}

\begin{code}
module LysKom.ProtocolA.Requests where
\end{code}

\begin{comment}
\begin{code}
import Text.ParserCombinators.Parsec
import LysKom.ProtocolA.Helpers
    ( whitespaces
    , word
    , withDelim
    , withWS
    , intParser )
import LysKom.ProtocolA.Types
\end{code}
\end{comment}

Communication with the server is specified through two forms.

\paragraph{Requests,} which detail queries and actions you can send to the server,

\begin{verbatim}
<procedure-name> [<N>] ( <REQUEST> ) -> ( <REPLY> ) ;
\end{verbatim}

and \paragraph{Asyncs,} which is data the server just pushes to you.

\begin{verbatim}
<async-name> [<N>] ( <DATA> )
\end{verbatim}

\subsection{Parser}

\begin{code}
fieldParser :: GenParser Char () (String, LysType)
fieldParser =
    (,) <$> withWS word <* char ':' <* whitespaces
        <*> lysTypeParser

requestAsyncCommon :: GenParser Char () (String, Int, [(String, LysType)])
requestAsyncCommon =
    (,,) <$> word
         <*> withDelim "[]" intParser
         <*> withDelim "()" (try (withDelim "()" $ sepBy fieldParser (char ';'))
                             <|> fmap (:[]) fieldParser
                             <|> return [])

uncurry3 :: (a -> b -> c -> d) -> (a,b,c) -> d
uncurry3 f (a,b,c) = f a b c

requestDeclarationParser :: GenParser Char () ProtocolAItem
requestDeclarationParser
    = uncurry3 Request
    <$> requestAsyncCommon
    <*> (string "->"
        *> withDelim "()" ( optionMaybe lysTypeParser )
        <* char ';')

asyncDeclarationParser :: GenParser Char () ProtocolAItem
asyncDeclarationParser = uncurry3 Async <$> requestAsyncCommon
\end{code}

