\begin{code}
module LysKom.ProtocolA where
\end{code}

\begin{comment}
\begin{code}
import Text.ParserCombinators.Parsec
import LysKom.ProtocolA.Helpers
    ( eol
    , whitespaces
    , word
    , withWS
    )
import LysKom.ProtocolA.Types
import LysKom.ProtocolA.Requests
\end{code}
\end{comment}

The protocol (obviously) exists of a series of these, giving us the top level parser

\begin{code}
documentParser :: GenParser Char () [ProtocolAItem]
documentParser = many $ withWS
    (   try derivedTypeParser
    <|> try commentParser
    <|> try protoEditionParser
    <|> try protoVerParser
    <|> try lysKomDVersionParser
    <|> try typeAliasParser
    <|> try requestAliasParser
    <|> try asyncAliasParser
    <|> try requestMetaParser
    <|> try requestDeclarationParser
    <|> try asyncDeclarationParser
    )
\end{code}

\section{Trivial parts}
The trivial of these are

\begin{description}
\item[types]
Anything on the form $key ::= value$.
\begin{code}
derivedTypeParser :: GenParser Char () ProtocolAItem
derivedTypeParser = DerivedType <$> (word <* withWS (string "::="))
                                <*> lysTypeParser
\end{code}

\item[comments] are self exlanatory
\begin{code}
commentParser :: GenParser Char () ProtocolAItem
commentParser = Comment <$> (char '#' *> whitespaces *> manyTill anyChar eol)
\end{code}

\item[version] strings. Mostly ignored
\begin{code}
protoEditionParser :: GenParser Char () ProtocolAItem
protoEditionParser
    = fmap ProtoEdition $ string "%PROTOEDITION" *> whitespaces *> word

protoVerParser :: GenParser Char () ProtocolAItem
protoVerParser
    = fmap ProtoVer $ string "%PROTOVER" *> whitespaces *> word

lysKomDVersionParser :: GenParser Char () ProtocolAItem
lysKomDVersionParser
    = fmap LysKomDVersion $ string "%LYSKOMDVERSION"
                            *> whitespaces
                            *> word
\end{code}

\item[aliases], which come in three variants, all handled through a simple helper.
\begin{code}
aliasParser :: String -> GenParser Char () (String, String)
aliasParser s = (,) <$> (string s *> whitespaces *> word)
                    <*> (whitespaces *> word)

typeAliasParser :: GenParser Char () ProtocolAItem
typeAliasParser = uncurry TypeAlias <$> aliasParser "%type-alias"

requestAliasParser :: GenParser Char () ProtocolAItem
requestAliasParser = uncurry RequestAlias <$> aliasParser "%request-alias"

asyncAliasParser :: GenParser Char () ProtocolAItem
asyncAliasParser = uncurry AsyncAlias <$> aliasParser "%async-alias"
\end{code}
\end{description}

\section{Request \& Async metadata}

Every request and async(see section~\ref{item:rpc}) declaration is preceded
by a short metadata-block, detailing whet it was added and if it's to
be used, on the form:

\begin{verbatim}
%Async: 22
    %Async-Name: async-text-aux-changed-11
    %Protocol version: 11
    %Status: Recommended
%End Async
\end{verbatim}

These aren't of particular interest, so we just slurp 'em up and forget 'em.

\begin{code}
requestMetaParser :: GenParser Char () ProtocolAItem
requestMetaParser = do
    type_ <- char '%' *> (string "Request" <|> string "Async")
    s <- manyTill anyChar (try $ string "%End" *> withWS (string type_))
    return . Other $ type_ ++ s
\end{code}
