module LysKom.ErrorCodes where

data ErrorCode
    = NoError | NotImplemented | ObsoleteCall | InvalidPassword
    | StringTooLong -- `error-status' indicates the maximum string length.
    | LoginFirst | LoginDisallowed | ConferenceZero
    | UndefinedConference -- `error-status' contains the conference number in question.
    | UndefinedPerson -- `error-status' contains the person number in question.
    | AccessDenied -- `error-status' indicates the object to which we didn't have enough permissions to.
    | PermissionDenied -- `error-status' indicated the object for which permission was lacking, or zero.
    | NotMember -- `error-status' indicates the conference in question.
    | NoSuchText -- `error-status' indicates the text number in question.
    | TextZero
    | NoSuchLocalText -- `error-status' indicates the offending number.
    | LocalTextZero
    | BadName
    | IndexOutOfRange -- `error-status' is undefined unless stated otherwise in the call documentation.
    | ConferenceExists | PersonExists | SecretPublic
    | Letterbox -- `error-status' indicates the conference number.
    | LdbError -- `error-status' is an internal code.
    | IllegalMisc -- `error-status' contains the index of the illegal item.
    | IllegalInfoType -- `error-status' is the type.
    | AlreadyRecipient -- `error-status' contains the recipient that already is.
    | AlreadyComment -- `error-status' contains the text number of the text that already is a comment.
    | AlreadyFootnote -- `error-status' contains the text number of the text that already is a footnote.
    | NotRecipient -- `error-status' contains the conference number in question.
    | NotComment -- `error-status' contains the text number that isn't a comment.
    | NotFootnote -- `error-status' contains the text number that isn't a footnote.
    | RecipientLimit -- `error-status' is the text that has the maximum number of recipients.
    | CommentLimit -- `error-status' is the text with the maximum number of comments.
    | FootnoteLimit -- `error-status' is the text with the maximum number of footnotes.
    | MarkLimit -- `error-status' is the text with the maximum number of marks.
    | NotAuthor  -- `error-status' contains the text number in question.
    | NoConnect | OutOfMemory | ServerIsCrazy | ClientIsCrazy
    | UndefinedSession -- `error-status' contains the offending session number.
    | RegexpError
    | NotMarked -- `error-status' indicates the text in question.
    | TemporaryFailure | LongArray | AnonymousRejected
    | IllegalAuxItem -- `error-status' contains the index in the aux-item list where the invalid item appears.
    | AuxItemPermission -- `error-status' contains the index at which the item appears in the aux-item list sent to the server.
    | UnknownAsync -- `error-status' contains the message type the server did not understand.
    | InternalError | FeatureDisabled | MessageNotSent
    | InvalidMembershipType -- `error-status' is undefined unless specifically mentioned in the documentation for a specific call.
    | InvalidRange | InvalidRangeList | UndefinedMeasurement
    | PriorityDenied -- `error-status' indicates the lowest priority that you have access to.
    | WeightDenied | WeightZero | BadBool
    | OtherError String
    deriving (Show, Eq)


instance Enum ErrorCode where
    fromEnum NoError               = 0
    fromEnum NotImplemented        = 2
    fromEnum ObsoleteCall          = 3
    fromEnum InvalidPassword       = 4
    fromEnum StringTooLong         = 5
    fromEnum LoginFirst            = 6
    fromEnum LoginDisallowed       = 7
    fromEnum ConferenceZero        = 8
    fromEnum UndefinedConference   = 9
    fromEnum UndefinedPerson       = 10
    fromEnum AccessDenied          = 11
    fromEnum PermissionDenied      = 12
    fromEnum NotMember             = 13
    fromEnum NoSuchText            = 14
    fromEnum TextZero              = 15
    fromEnum NoSuchLocalText       = 16
    fromEnum LocalTextZero         = 17
    fromEnum BadName               = 18
    fromEnum IndexOutOfRange       = 19
    fromEnum ConferenceExists      = 20
    fromEnum PersonExists          = 21
    fromEnum SecretPublic          = 22
    fromEnum Letterbox             = 23
    fromEnum LdbError              = 24
    fromEnum IllegalMisc           = 25
    fromEnum IllegalInfoType       = 26
    fromEnum AlreadyRecipient      = 27
    fromEnum AlreadyComment        = 28
    fromEnum AlreadyFootnote       = 29
    fromEnum NotRecipient          = 30
    fromEnum NotComment            = 31
    fromEnum NotFootnote           = 32
    fromEnum RecipientLimit        = 33
    fromEnum CommentLimit          = 34
    fromEnum FootnoteLimit         = 35
    fromEnum MarkLimit             = 36
    fromEnum NotAuthor             = 37
    fromEnum NoConnect             = 38
    fromEnum OutOfMemory           = 39
    fromEnum ServerIsCrazy         = 40
    fromEnum ClientIsCrazy         = 41
    fromEnum UndefinedSession      = 42
    fromEnum RegexpError           = 43
    fromEnum NotMarked             = 44
    fromEnum TemporaryFailure      = 45
    fromEnum LongArray             = 46
    fromEnum AnonymousRejected     = 47
    fromEnum IllegalAuxItem        = 48
    fromEnum AuxItemPermission     = 49
    fromEnum UnknownAsync          = 50
    fromEnum InternalError         = 51
    fromEnum FeatureDisabled       = 52
    fromEnum MessageNotSent        = 53
    fromEnum InvalidMembershipType = 54
    fromEnum InvalidRange          = 55
    fromEnum InvalidRangeList      = 56
    fromEnum UndefinedMeasurement  = 57
    fromEnum PriorityDenied        = 58
    fromEnum WeightDenied          = 59
    fromEnum WeightZero            = 60
    fromEnum BadBool               = 61
    fromEnum (OtherError _)        = -1

    toEnum   0 = NoError
    toEnum   2 = NotImplemented
    toEnum   3 = ObsoleteCall
    toEnum   4 = InvalidPassword
    toEnum   5 = StringTooLong
    toEnum   6 = LoginFirst
    toEnum   7 = LoginDisallowed
    toEnum   8 = ConferenceZero
    toEnum   9 = UndefinedConference
    toEnum  10 = UndefinedPerson
    toEnum  11 = AccessDenied
    toEnum  12 = PermissionDenied
    toEnum  13 = NotMember
    toEnum  14 = NoSuchText
    toEnum  15 = TextZero
    toEnum  16 = NoSuchLocalText
    toEnum  17 = LocalTextZero
    toEnum  18 = BadName
    toEnum  19 = IndexOutOfRange
    toEnum  20 = ConferenceExists
    toEnum  21 = PersonExists
    toEnum  22 = SecretPublic
    toEnum  23 = Letterbox
    toEnum  24 = LdbError
    toEnum  25 = IllegalMisc
    toEnum  26 = IllegalInfoType
    toEnum  27 = AlreadyRecipient
    toEnum  28 = AlreadyComment
    toEnum  29 = AlreadyFootnote
    toEnum  30 = NotRecipient
    toEnum  31 = NotComment
    toEnum  32 = NotFootnote
    toEnum  33 = RecipientLimit
    toEnum  34 = CommentLimit
    toEnum  35 = FootnoteLimit
    toEnum  36 = MarkLimit
    toEnum  37 = NotAuthor
    toEnum  38 = NoConnect
    toEnum  39 = OutOfMemory
    toEnum  40 = ServerIsCrazy
    toEnum  41 = ClientIsCrazy
    toEnum  42 = UndefinedSession
    toEnum  43 = RegexpError
    toEnum  44 = NotMarked
    toEnum  45 = TemporaryFailure
    toEnum  46 = LongArray
    toEnum  47 = AnonymousRejected
    toEnum  48 = IllegalAuxItem
    toEnum  49 = AuxItemPermission
    toEnum  50 = UnknownAsync
    toEnum  51 = InternalError
    toEnum  52 = FeatureDisabled
    toEnum  53 = MessageNotSent
    toEnum  54 = InvalidMembershipType
    toEnum  55 = InvalidRange
    toEnum  56 = InvalidRangeList
    toEnum  57 = UndefinedMeasurement
    toEnum  58 = PriorityDenied
    toEnum  59 = WeightDenied
    toEnum  60 = WeightZero
    toEnum  61 = BadBool
    toEnum (-1) = OtherError ""
