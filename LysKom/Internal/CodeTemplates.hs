{-# LANGUAGE TemplateHaskell, ImportQualifiedPost  #-}

module LysKom.Internal.CodeTemplates
    ( decsFor
    , asyncBuild
    ) where

import Control.Exception (throw, PatternMatchFail (..))

import Data.Attoparsec.ByteString (choice)
import Data.Attoparsec.ByteString.Char8 (char, string, decimal)

import Data.ByteString.Builder (char7, intDec, string7)
import Data.ByteString.Char8 (pack)

import Data.Functor (($>))
import Data.List (intersperse)
import Data.List qualified as L
import Data.Word (Word32, Word16, Word8)

import Language.Haskell.TH hiding (bang)

import LysKom.ProtocolA.Types
import LysKom.WireFormat


lystype :: LysType -> Type
lystype INT32     = ConT ''Word32
lystype INT16     = ConT ''Word16
lystype INT8      = ConT ''Word8
lystype BOOL      = ConT ''Bool
lystype FLOAT     = ConT ''Float
lystype HOLLERITH = ConT ''String
lystype (ARRAY n) = AppT ListT $ lystype n
lystype (Reference n) = ConT . mkName $ n
lystype s = throw $
    PatternMatchFail $ "lystype should NEVER be called on compound types: "
                        ++ show s


-- arrow [a, b, c] ⇒ a -> b -> c
arrow :: [Type] -> Type
arrow (x:[]) = x
arrow (x:xs) = AppT (AppT ArrowT x) $ arrow xs

-- Apply infix operator
-- Joins our list of expressions by an infix operator
apInfix :: Exp -> [Exp] -> Exp
apInfix _  (x:[]) = x
apInfix op (x:xs) = UInfixE x op $ apInfix op xs


bang :: Bang
bang = Bang NoSourceUnpackedness NoSourceStrictness

derivShow :: DerivClause
derivShow = DerivClause Nothing [ ConT ''Show ]

-- Delarations for.
-- Compile (almost) every Protocol A item to its coresponding list of
-- Haskell declarations.) Includes declaring datatypes and required
-- instances.
decsFor :: ProtocolAItem -> Q [Dec]

-- comments are obviously ignored
decsFor (Comment _) = return []

-- asyncs are handled separately since they all are joined in a single
-- data type. This leads to us ignoring it here.
decsFor (Async _ _ _)      = return []

-- these contain some metadata about the version of the protocol.
-- There is some interesting data to extract from them, but we can
-- safely ignore it.
decsFor (ProtoEdition _)   = return []
decsFor (ProtoVer _)       = return []
decsFor (LysKomDVersion _) = return []
decsFor (Other _)          = return []

-- LysKom types are created with their "pretty" names, to allow
-- pattern match on the pretty names.  This creates type aliases with
-- the stable names.
decsFor (TypeAlias old new)    = return [ TySynD (mkName new) []
                                    $ ConT (mkName old) ]

-- AsyncAliases are left out. They are created with their pretty names
-- (instead of their stable names) to allow easier pattern matching.
-- Type constructor aliases could be created, but but since are we
-- currently only focused on clients (and not servers) we have no use
-- for them.
decsFor (AsyncAlias _ _) = return []

-- requests are defined by their stable name, this binds the pretty
-- names to mean the same thing.
-- For example
-- enable = enable1
decsFor (RequestAlias old new) = return [ ValD (VarP . mkName $ new)
                                    (NormalB . VarE . mkName $ old)
                                    [] ]

-- We map all type declarations to their coresponding Haskell type
-- declaration.

-- Typse which is nothing more than an alternative name for an
-- existing type are mapped as haskell type aliases. Note that the
-- same is true for the primitive types, but they are handled further
-- down since they are the catch-all clause for the DerivedType
-- pattern match.
decsFor (DerivedType name (Reference other_name)) = return [
    TySynD (mkName name) [] $
        ConT (mkName other_name) ]

decsFor (DerivedType name (Union (a, b))) = return
    [ TySynD (mkName name) [] $
          foldl AppT (ConT ''Either)
               [ lystype $ a
               , lystype $ b ] ]

-- ENUMERATION_OF creates an enumeration from a given SELECTION.
-- These are converted to ENUMERATION expressions in an earlier pass
-- of the tree, and should therefore NEVER appear here.
-- decsFor (DerivedType name (ENUMERATION_OF t)) = return []

decsFor (DerivedType name (ENUMERATION t)) = do
    let className = mkName name
    let pairs = [ ( mkName n
                  , IntegerL . toInteger $ i)
                | (n, i) <- t ]

    decoderBodies <- sequence [ [| ( string . pack . show $ $(return . LitE $ i) )
                                    $> $(return . ConE $ n) |]
                              | (n, i) <- pairs ]

    let enc = mkName "encode"
    return [ DataD [] className [] Nothing
                 [ NormalC n [] | (n,_) <- pairs ]
                 [ derivShow ]
           , InstanceD Nothing [] (AppT (ConT ''WireFormat) (ConT className)) $
               [ ValD (VarP . mkName $ "decoder")
                      (NormalB (AppE (VarE 'choice) $ ListE decoderBodies))
                      []
               , ValD (VarP enc)
                      (NormalB $ UInfixE (VarE enc)
                                    (VarE . mkName $ ".")
                                    (VarE . mkName $ "fromEnum"))
                      []]
           , InstanceD Nothing [] (AppT (ConT ''Enum) (ConT className))
               [ FunD (mkName "fromEnum")
                   [ Clause [ ConP n [] ] (NormalB . LitE $ i) []
                   | (n, i) <- pairs ]
               , FunD (mkName "toEnum")
                   [ Clause [ LitP i ] (NormalB . ConE $ n) []
                   | (n, i) <- pairs ] ] ]


decsFor (DerivedType name (SELECTION opts)) = do

    let className = mkName name

    options <- (flip mapM) opts $ \(i,fn,(n,t)) -> do
        fieldname <- newName fn
        let constructorName = mkName n
        body <- [| intDec i <> char7 ' ' <> encode $(return . VarE $ fieldname) |]
        return ( constructorName, i, fieldname, lystype t, body )

    s <- fmap ListE . sequence $
        [ [| $(return . ConE $ n) `fmap` ((string . pack $
                                            $(return . LitE . StringL . show $ i) )
                                   *> char ' ' *> decoder) |]
        | (n,i,_,_,_) <- options ]
    decoderBody <- [| choice $ $(return s) |]

    let constructors = [ NormalC cn [(bang, t)]
                       | (cn,_,_,t,_) <- options ]


    return [ DataD [] className [] Nothing constructors [ derivShow ]
           , InstanceD Nothing [] (AppT (ConT ''WireFormat) (ConT className))
                [ FunD (mkName "encode")
                    [ Clause [ ConP n $ [ VarP fn ] ]
                        (NormalB body) []
                    | (n, _, fn, _, body) <- options ]
                , FunD (mkName "decoder")
                    [ Clause [] (NormalB decoderBody) [] ]
                ]]



decsFor (DerivedType structureName (STRUCTURE fields)) = do
    let name = mkName structureName
    gens <- mapM newName $ fst <$> fields
    body <- [| mconcat . intersperse (char7 ' ') $
                $(return . ListE $ (AppE . VarE $ 'encode) . VarE <$> gens) |]

    let chr = (AppE (VarE 'char) (LitE . CharL $ ' '))
    return [ DataD [] name [] Nothing
               [RecC name [(mkName n, bang, lystype t) | (n, t) <- fields]]
               [ derivShow ]
           , InstanceD Nothing [] (AppT (ConT ''WireFormat) (ConT name))
                 [ FunD (mkName "encode")
                     [ Clause [ ConP name $ map VarP gens ]
                           (NormalB body)
                           [] ]
                , FunD (mkName "decoder")
                    [ Clause []
                        (NormalB $
                            UInfixE (ConE name) (VarE . mkName $ "<$>")
                                $ apInfix (VarE . mkName $ "<*>") $
                                    (VarE 'decoder)
                                        : (take (length fields - 1) $
                                                repeat $
                                                    ParensE (
                                                    UInfixE chr (VarE . mkName $ "*>")
                                                        (VarE 'decoder) )))

                        [] ] ] ]


decsFor (DerivedType stringName (BITSTRING fields)) = do
    let name = mkName stringName
    gens <- mapM newName fields
    body <- [| mconcat $ $(return . ListE $ (AppE . VarE $ 'encode) . VarE <$> gens) |]

    let ap = VarE . mkName $ "<*>"
    return [ DataD [] name [] Nothing
              [RecC name [(mkName n, bang, ConT ''Bool) | n <- fields]]
              [ derivShow ]
           , InstanceD Nothing [] (AppT (ConT ''WireFormat) (ConT name))
                 [ FunD (mkName "encode")
                     [ Clause [ ConP name $ map VarP gens ]
                           (NormalB body)
                           [] ]
                 , FunD 'decoder
                     [ Clause []
                        (NormalB $
                            UInfixE (ConE name) (VarE . mkName $ "<$>")
                                $ apInfix ap $ take (length fields)
                                                    (repeat $ VarE 'decoder))
                        [] ]]
           ]


-- Primitive types + array
-- See comment for DerivedType name Reference
decsFor (DerivedType name t) = return [TySynD (mkName name) [] $ lystype t]


decsFor (Request name num args ret) = do
    let nn = mkName name
    let lit = return . LitE . StringL . show
    syms <- sequence [ newName n | (n,_) <- args ]
    sp <- [| char7 ' ' |]
    body <- case syms of
            [] -> [| request (string7 $(lit num)) |]
            _ ->  [| request (string7 $(lit num)
                                 <> char7 ' '
                                 <> $(return $ apInfix (VarE . mkName $ "<>")
                                                 $ L.intersperse sp $
                                                    (map (AppE (VarE 'encode))
                                                         (map VarE syms)))) |]

    return $
        [ SigD nn $
                arrow $ [ lystype t | (_,t) <- args ]
                     ++ [ AppT (ConT . mkName $ "LysKom")
                            $ maybe (TupleT 0) lystype ret ]
        , FunD nn [Clause (map VarP syms) (NormalB body) []]
        ]




-- Similiar to decsFor, but looks at all async async declarations in
-- Protocol A at once. Required to be standalone since all asyncs form
-- a single data type.
asyncBuild :: [ProtocolAItem] -> Q [Dec]
asyncBuild asyncDeclarations = do

    let typename = mkName "KomAsync"

    let sp = AppE (VarE 'char) (LitE . CharL $ ' ')
    let lit = LitE . StringL . show
    let ap = (VarE '(*>))

    let header i = ParensE $ apInfix ap [ VarE 'decimal
                                        , sp
                                        , AppE (VarE 'string) $ AppE (VarE 'pack) (lit i)
                                        , sp
                                        , VarE 'decoder ]

    let db = [ if null t
               then UInfixE
                       (ParensE $ apInfix ap [ VarE 'decimal
                                             , sp
                                             , AppE (VarE 'string) $ AppE (VarE 'pack) (lit i) ])
                       (VarE '($>))
                       (ConE . mkName $ n)

               else UInfixE (ConE . mkName $ n)
                               (VarE . mkName $ "<$>")
                               $ apInfix (VarE . mkName $ "<*>")
                                         $ header i : (take (length t - 1) $
                                                     repeat $ ParensE (UInfixE sp ap $ VarE 'decoder))
             | (Async n i t) <- asyncDeclarations ]


    return
        [ DataD [] typename [] Nothing
            [ RecC (mkName name) [ (mkName fn, bang, lystype t)
                                 | (fn, t) <- fields ]
            | (Async name _ fields) <- asyncDeclarations ]
            [ derivShow ]

        , InstanceD Nothing [] (AppT (ConT ''WireFormat) (ConT typename))
              [ ValD (VarP 'decoder)
                     (NormalB (AppE (VarE 'choice) $ ListE db))
                     []
              , ValD (VarP 'encode) (NormalB $ VarE 'undefined) [] ]]

