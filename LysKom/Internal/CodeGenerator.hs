module LysKom.Internal.CodeGenerator
    ( spliceGeneratedCode
    ) where

import Control.Exception (Exception, throw)

import Language.Haskell.TH (runIO, Q, Dec)
import Language.Haskell.TH.Syntax (sequenceQ)

import LysKom.Internal.CodeTemplates (decsFor, asyncBuild)
import LysKom.Internal.NameTransform (transformTree)
import LysKom.ProtocolA (documentParser)
import LysKom.ProtocolA.Types
    ( LysType ( ENUMERATION_OF
              , ENUMERATION
              , SELECTION )
    , ProtocolAItem (..))

import Text.ParserCombinators.Parsec (parseFromFile, ParseError)

-- This allows us to later throw ParseErrors. Useful since a
-- parse error when reading Protocol A is an error, and will require
-- a human to fix it.
instance Exception ParseError


isSelect :: String -> ProtocolAItem -> Bool
isSelect m (DerivedType n _) = n == m
isSelect _ _                 = False

isAsync :: ProtocolAItem -> Bool
isAsync (Async _ _ _) = True
isAsync _             = False

enumOf2Enum :: [ProtocolAItem] -> ProtocolAItem -> ProtocolAItem
enumOf2Enum lst (DerivedType n (ENUMERATION_OF t))
    = let (DerivedType _ (SELECTION xs)) = head $ filter (isSelect t) lst
          enum = ENUMERATION [ (n ++ "E", i) | (i, _, (n,_)) <- xs ]
      in  DerivedType n enum
enumOf2Enum _ a = a

-- A rather ugly filter to remove parts of the protocol
-- Note that this runs AFTER my name transform, meaning that the
-- searched for names are the haskell-appropriate versions
g :: ProtocolAItem -> Bool
-- filter out lookup-name since its target is non-existant
g (RequestAlias "lookupName1" _) = False
-- typeclash for common fieldname `textStat' with AsyncDeletedText
g (Async "AsyncNewTextOld" _ _) = False
g _ = True


-- Loads the protocol specification from a file, and generate a list
-- of Haskell declarations
spliceGeneratedCode :: FilePath -> Q [Dec]
spliceGeneratedCode protApath = do
    d <- runIO $ parseFromFile documentParser protApath
    result <- case d of
                Left err -> throw err
                Right result -> return result

    let dat = transformTree $ result

    let asyncs = filter g $ filter isAsync dat
    let dat'   = filter g $ map (enumOf2Enum dat) dat

    mconcat <$> sequenceQ (asyncBuild asyncs : map decsFor dat')
