module LysKom.Internal.NameTransform
( transformTree ) where

import qualified Data.Map.Lazy as Map

import Data.Maybe (fromMaybe, catMaybes)
import Data.Char (toUpper)

import LysKom.ProtocolA.Types

type VarName  = String
type TypeName = String

-- varname "last-time-read" ⇒ "lastTimeRead"
varname :: String -> VarName
varname [] = ""
-- data and type are reserved in haskell, this is next best thing
varname "data" = "data'"
varname "type" = "type'"
-- name conflict with between requests and fields.
-- rename these and let requests keep their "proper" names
varname "change-name" = "changeName'"
varname "create-conf" = "createConf'"
-- one would think that (newName s) would produce always produce a
-- valid name. And (newName "where") pretty prints as where_1, which
-- seems like a valid name. However, internally the variable is still
-- named "where", which is an illegal variable name.
varname "where" = "where'"
varname ('-':c:xs) = toUpper c : varname xs
varname (c:xs) = c : varname xs

fname :: String -> VarName
fname ('-':c:xs) = toUpper c : varname xs
fname (c:xs) = c : varname xs
fname [] = ""


-- tn handles typename transformations inside LysType's.
-- This is more or less a type specific fmap.
tn :: (String -> TypeName) -> LysType -> LysType
tn _        (BITSTRING n)      = BITSTRING $ map varname n
tn typename (ENUMERATION_OF n) = ENUMERATION_OF $ typename n
tn typename (ENUMERATION n)    = ENUMERATION $ [(typename a, i) | (a,i) <- n]
tn typename (STRUCTURE n)      = STRUCTURE [(varname a, tn typename b) | (a, b) <- n]
-- document SELECTION choices
tn typename (SELECTION n)      = SELECTION [(i, varname a, (typename b, tn typename c)) | (i, a, (b, c)) <- n]
tn typename (ARRAY n)          = ARRAY $ tn typename n
tn typename (Reference n)      = Reference $ typename n
tn typename (Union (a,b))      = Union (tn typename a, tn typename b)
tn _ x = x

-- Transform a given Protocol A item. First argument should transform
-- Protocol A names into valid typenames.
transformNames :: (String -> TypeName) -> ProtocolAItem -> ProtocolAItem

-- These don't contain any names
transformNames _ x@(Comment _)        = x
transformNames _ x@(ProtoEdition _)   = x
transformNames _ x@(ProtoVer _)       = x
transformNames _ x@(LysKomDVersion _) = x
transformNames _ x@(Other _)          = x

transformNames typename (DerivedType name v) = DerivedType (typename name) (tn typename v)

transformNames typename (Request name a xs ret)
    = Request (fname name)
              a
              [(varname x, tn typename y) | (x,y) <- xs]
              (tn typename <$> ret)

-- The name of asyncs fields are all suffixed by an underscore to not
-- collide with structure field names.
transformNames typename (Async name a xs)
    = Async (typename name) a [(varname x ++ "_", tn typename y) | (x,y) <- xs]

transformNames _ (RequestAlias a b) = RequestAlias (fname a) (fname b)

-- We flip the primitive and the pretty name for Type- and
-- AsyncAliases pattern matching will happen on the non-aliased name,
-- and we want the pretty name in our type aliases.
transformNames typename (TypeAlias prim pretty)
    = TypeAlias (typename pretty) (typename' prim)

transformNames typename (AsyncAlias a b)
    = AsyncAlias (typename b) (typename' a)

-- async-i-am-off ⇒ AsyncIAmOff
-- typename "sparse-block" ⇒ "SparseBlock"
typename' :: String -> TypeName
typename' [] = []
typename' (s:xs) = toUpper s : f xs
    where f [] = []
          f ('-':c:cs) = toUpper c : f cs
          f (c:cs) = c : f cs

taAsPair :: ProtocolAItem -> Maybe (TypeName, TypeName)
taAsPair (TypeAlias  a b) = Just (a,b)
taAsPair (AsyncAlias a b) = Just (a,b)
taAsPair _ = Nothing

-- Perform transformations on all names in the list of protocol A
-- items. Ensures that everything is valid Haskell names, that we use
-- the correct versions, and prevent name collisions.
-- Constructs a name transformer procedure which ensures that types
-- get their pretty name.
transformTree :: [ProtocolAItem] -> [ProtocolAItem]
transformTree tree = (transformNames f) <$> tree
    where aliasMap = Map.fromList $ catMaybes (map taAsPair tree)
          f name = typename' $ fromMaybe name (Map.lookup name aliasMap)
