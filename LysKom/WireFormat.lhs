\begin{code}

-- allow instances on lists.
-- used both for the String instance, but also to generalize array intances
{-# LANGUAGE FlexibleInstances #-}

-- Direct string s for \emph{both} ByteString.Builder and Attoparsec
{-# LANGUAGE OverloadedStrings #-}

module LysKom.WireFormat where

import Data.ByteString.Builder
import qualified Data.ByteString.Lazy as BS
import Data.Attoparsec.ByteString
    ( Parser
    , choice
    , count
    , eitherP )
import Data.Attoparsec.ByteString.Char8
    ( char
    , anyChar
    , rational
    , decimal )
import Data.Functor (($>))
import Data.Word

class WireFormat a where
    encode :: a -> Builder
    decoder :: Parser a

instance WireFormat Bool where
    encode True  = char7 '1'
    encode False = char7 '0'
    decoder = toEnum . read . pure <$> choice [char '0', char '1']

instance WireFormat Word8 where
    encode = word8Dec
    decoder = decimal

instance WireFormat Word16 where
    encode = word16Dec
    decoder = decimal

instance WireFormat Word32 where
    encode = word32Dec
    decoder = decimal

instance WireFormat Float where
    -- TODO ensure this is equivalent to printf("%g", val)
    encode = floatDec
    decoder = rational

-- Strings are NOT character lists in LysKom, intoduce overlapping instance
instance {-# OVERLAPPING #-} WireFormat [Char] where
    -- TODO string8 encodes as Latin1, but badly.
    -- truncating multi-byte characters instead of actually encoding them.
    encode s = let bs = toLazyByteString $ string8 s
               in int64Dec (BS.length bs)
               <> "H" <> lazyByteString bs

    -- The situation is similar for anyChar. It decodes Latin1(5), but badly.
    decoder = do
        len <- decimal
        char 'H' *> count len anyChar


instance WireFormat a => WireFormat [a] where
    encode t = intDec (length t)
            <> " { " <> mconcat ((<> " ") . encode <$> t) <> "}"

    decoder = do
        len <- decimal
        " " *> choice
            [ char '*' $> []
            , "{ " *> count len (decoder <* " ") <* "}" ]

----------------------------------------

instance WireFormat () where
    encode () = ""
    decoder = return ()

instance WireFormat Int where
    encode = intDec
    decoder = decimal

instance (WireFormat a, WireFormat b) => WireFormat (Either a b) where
    encode (Left  a) = encode a
    encode (Right a) = encode a
    decoder = eitherP decoder decoder

\end{code}
